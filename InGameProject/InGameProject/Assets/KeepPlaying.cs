﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepPlaying : MonoBehaviour {

    private void Awake()
    {
        //IF THERE IS A MUSIC PLAYER DO NOT CREATE ANOTHER ON RELOAD SCENE
        GameObject[] objs = GameObject.FindGameObjectsWithTag("music");
        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        //DO NOT DESTROY THE PLAYER ON SCENE CHANGE
        DontDestroyOnLoad(this.gameObject);
    }
}
