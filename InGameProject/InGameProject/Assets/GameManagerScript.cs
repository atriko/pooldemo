﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

//SCOREBOARD CLASS FOR STORING SCORE AND TIME
public class Scoreboard
{
    public int score;
    public string time;

    public Scoreboard(int _score, string _time)
    {
        score = _score;
        time = _time;
    }
}

public class GameManagerScript : MonoBehaviour {

    //VARIABLES FOR HIT NOTICE ON BALLS
    public bool redBallHit = false;
    public bool yellowBallHit = false;
   

    //GENERATING RIGIDBODY OF THE BALLS
    public Rigidbody playerBallrb;
    public Rigidbody redBallrb;
    public Rigidbody yellowBallrb;

    public GameObject replayButton;

    //GENERATING UI ELEMENTS
    public Text scoreTextinGame;
    public Text timerTextinGame;
    public Text readyText;
    public GameObject readyPanel;
    public AudioSource sound;

    //VARIABLES
    public int endingScore;
    public bool readyState;
    public bool replayStatus;

    private PlayerScript player;
    private int score = 0;
    private string jsonString;
    private string path;
    

    private void Start()
    {
        path = Application.dataPath + "/JSONscores.json";
    }
    //GETTING THE INFORMATIONS FOR BALL COLLISIONS
    public void RedBallHit()
    {
        redBallHit = true;
    }
    public void YellowBallHit()
    {
        yellowBallHit = true;
    }
    public void ResetHitNotice()
    {
        redBallHit = false;
        yellowBallHit = false;
    }
    public void PlayHitSound(float magnitude)
    {
        sound.volume = magnitude / 10;
        sound.Play();
    }


    //CHANGING UI PANEL FOR READY 
    public void MakeItReady()
    {
        readyText.text = string.Format("Ready");
        readyPanel.GetComponent<Image>().color = new Color32(0, 255, 30, 143);
        readyState = true;
    }
    //CHANGING UI PANEL FOR NOT READY
    public void MakeItNotReady()
    {
        readyPanel.GetComponent<Image>().color = new Color32(255, 20, 0, 143);
        readyText.text = string.Format("Not Ready");
        readyState = false;
    }
    //CHANGING UI PANEL FOR REPLAY
    public void MakeItReplay()
    {

        readyPanel.GetComponent<Image>().color = new Color32(0, 30, 255, 143);
        readyText.text = string.Format("Replay");
        readyState = false;
        
    }
    public void ExitReplay()
    {
        //ZERO OUT ALL THE BALL VARIABLES
        replayStatus = false;
        playerBallrb.velocity = Vector3.zero;
        playerBallrb.angularVelocity = Vector3.zero;
        redBallrb.velocity = Vector3.zero;
        redBallrb.angularVelocity = Vector3.zero;
        yellowBallrb.velocity = Vector3.zero;
        yellowBallrb.angularVelocity = Vector3.zero;

        //PUT THE BALLS TO THE POSITIONS BEFORE REPLAY
        playerBallrb.position = replayButton.GetComponent<ReplayButton>().replayPosPlayer;
        redBallrb.position = replayButton.GetComponent<ReplayButton>().replayPosRed;
        yellowBallrb.position = replayButton.GetComponent<ReplayButton>().replayPosYellow;
        
    }
    //CREATE THE SCORE BOARD OBJECT TO BE SAVED IN JSON
    private void SaveScoreAndEnd()
    {
        Scoreboard playerScore = new Scoreboard(score, timerTextinGame.text);
        jsonString = JsonUtility.ToJson(playerScore);

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(jsonString);
            }
        }
        Invoke("EndGame", 2);
    }
    //LOAD THE END GAME SCENE
    private void EndGame()
    {
        SceneManager.LoadScene(2);
    }
    
    void Update () {
        //IF BOTH BALLS ARE GOT HIT ITS A SCORE
        if (redBallHit==true && yellowBallHit==true)
        {
            if (replayStatus == false)
            {
                score++;
            }
            scoreTextinGame.text = string.Format("Score : {0}", score);
            redBallHit = false;
            yellowBallHit = false;
        }
        //IF SCORE EQUALS THE ENDING SCORE FINISH THE GAME
        if (score == endingScore)
        {
            SaveScoreAndEnd();
        }


    }

    
}