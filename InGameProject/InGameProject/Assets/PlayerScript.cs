﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//CLASS OF LAST SITUATION FOR STORING LAST POSITIONS OF THE BALLS AND THE LAST FORCE
public class LastSituation
{
    public Vector3 redPos;
    public Vector3 yellowPos;
    public Vector3 playerPos;
    public Vector3 force;

    public LastSituation(Vector3 _redPos, Vector3 _yellowPos, Vector3 _playerPos, Vector3 _force)
    {
        redPos = _redPos;
        yellowPos = _yellowPos;
        playerPos = _playerPos;
        force = _force;
    }

}
public class PlayerScript : MonoBehaviour {

    //LOADING CAMERA
    public Transform cam;

    //LOADING BALL OBJECTS
    public GameObject playerBallObj;
    public GameObject redBallObj;
    public GameObject yellowBallObj;

    //LOADING GAME MANAGER OBJECT
    public GameObject gm;

    //LOADING UI ELEMENTS
    public Slider slider;
    public AudioSource sound;

    public Vector3 lastForce;
    public LastSituation lastSit;

    //VARIABLES
    private float maxForce = 100f;
    private float minForce = 10f;
    private float chargeForce;
    private float sliderval;

    //BALL RIGIDBODY COMPENENT
    private Rigidbody playerBall;
    private Rigidbody redBall;
    private Rigidbody yellowBall;
    

    private void Start()
    {
        //RIGIDBODY INITIALIZATION
        playerBall = playerBallObj.GetComponent<Rigidbody>();
        redBall = redBallObj.GetComponent<Rigidbody>();
        yellowBall = yellowBallObj.GetComponent<Rigidbody>();
    }
    // USING FIXED UPDATE FOR PHYSICS UPDATES
    void FixedUpdate () {
        //IF ITS NOT ON REPLAY MODE
        if (gm.GetComponent<GameManagerScript>().replayStatus == false)
        {
            // IF ALL THE BALLS ARE STATIONARY
            if (playerBall.velocity.magnitude < 0.01 && redBall.velocity.magnitude < 0.01 && yellowBall.velocity.magnitude < 0.01)
            {
                //TELL GM IT'S READY
                gm.SendMessage("MakeItReady");
                //RESETTING HIT COUNTERS
                gm.SendMessage("ResetHitNotice");

                //WHILE CHARGING SPACE
                if (Input.GetButton("space"))
                {
                    //CHARGE POWER BY TIME
                    if (chargeForce < maxForce)
                    {
                        chargeForce += Time.deltaTime * 100f;
                    }
                    //LIMIT THE MAX POWER
                    else
                    {
                        chargeForce = maxForce;
                    }
                    //UPDATE SLIDER WHILE CHARGING
                    sliderval = chargeForce / maxForce;
                    slider.value = sliderval;

                    //PLAY SOUND OF HITTING WHEN YOU FINISH
                    sound.volume = sliderval;
                    sound.Play();
                }
                else //RELEASING SPACE
                {
                    if (chargeForce > 0f)
                    {
                        chargeForce = chargeForce + minForce;
                        //CREATING VECTOR3 FOR AIM FROM THE CAMERA X AND Z
                        Vector3 aim = new Vector3(cam.forward.x, 0, cam.forward.z);
                        //MULTIPLY WITH THE CHARGEFORCE TO GET THE LAST FORCE
                        lastForce = aim * chargeForce;

                        //BEFORE YOU APPLY FORCE STORE ALL POSITIONS AND FORCE IN lastSit OBJECT
                        lastSit = new LastSituation(redBall.position, yellowBall.position, playerBall.position, lastForce);

                        //ADDING FORCE
                        playerBall.AddForce(lastForce);

                        //RETURN SLIDER AND CHARGE VALUES TO ZERO
                        chargeForce = 0f;
                        slider.value = 0f;
                    }

                }
            }
            else //IF TE BALLS ARE NOT STATIONARY 
            {
                gm.SendMessage("MakeItNotReady");
            }
        }
        
    }
}
