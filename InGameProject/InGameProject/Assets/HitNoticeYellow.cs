﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitNoticeYellow : MonoBehaviour {

    public GameObject gm;
    public AudioSource sound;
    private void OnCollisionEnter(Collision collision)
    {
        // IF THE COLLIDER IS PLAYER ALERT GAME MANAGER
        if (collision.collider.tag == "player")
        {
            gm.SendMessage("YellowBallHit");

        }
        //ALERT GAME MANAGER TO MAKE A SOUND
        gm.SendMessage("PlayHitSound", collision.relativeVelocity.magnitude);
    }
    
}
