﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timerText;
    public GameObject gameManager;

    private float time;

    
	// Update is called once per frame
	void Update () {
        // DO NOT COUNT WHILE IN REPLAY
        if (gameManager.GetComponent<GameManagerScript>().replayStatus == false)
        {
            time += Time.deltaTime;
        }

        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        timerText.text = string.Format("{0:00.} : {1:00.}",minutes,seconds);
	}
}
