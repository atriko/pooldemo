﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplayButton : MonoBehaviour {

    public GameObject playerObject;
    public GameObject replay;
    public GameObject gameManager;

    public Rigidbody playerBall;

    public Transform playerPos;
    public Transform redPos;
    public Transform yellowPos;
    

    private Button replayButton;
    private PlayerScript player;
    private Vector3 replayForce;

    public Vector3 replayPosPlayer;
    public Vector3 replayPosRed;
    public Vector3 replayPosYellow;

    private Vector3 firstPos;
    
    public void PlayReplay()
    {
        //DISABLE THE REPLAY BUTTON AFTER PRESS
        replayButton.interactable = false;
        //MAKING THE REPLAY STATUS TRUE
        gameManager.GetComponent<GameManagerScript>().replayStatus = true;

        //STORE POSITIONS OF THE BALLS BEFORE REPLAY
        replayPosPlayer = playerPos.position;
        replayPosRed = yellowPos.position;
        replayPosYellow = redPos.position;

        //PUT THE BALLS ON THE LAST POSITIONS
        playerPos.position = player.lastSit.playerPos;
        redPos.position = player.lastSit.redPos;
        yellowPos.position = player.lastSit.yellowPos;
        //GET THE LAST FORCE APPLIED
        replayForce = player.lastSit.force;
        //TELL THE MANAGER WE ARE IN THE REPLAY MODE
        gameManager.SendMessage("MakeItReplay");
        //AFTER PUTTING BALLS WAIT 2 SECONDS BEFORE PLAYING
        HitReplay();

        
        
    }
    public void HitReplay()
    {

        //APPLY THE SAME FORCE ON THE BALL
        playerBall.AddForce(replayForce);


    }
    // Update is called once per frame
    void Start()
    {
        replayButton = replay.GetComponent<Button>();
        player = playerObject.GetComponent<PlayerScript>();
    }
    void Update () {
        if (gameManager.GetComponent<GameManagerScript>().readyState == false)
        {
            replayButton.interactable = false;
        }
        else
        {
            replayButton.interactable = true;
        }

	}
}
