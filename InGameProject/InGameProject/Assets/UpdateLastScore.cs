﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class UpdateLastScore : MonoBehaviour {

    public Text lastScore;
    public Text lastTime;

    private string path;
    
	// Use this for initialization
	private void Start () {

        path = Application.dataPath + "/JSONscores.json";

        if (File.Exists(path))
        {
            Debug.Log("exists");
            string dataAsJson = File.ReadAllText(path);
            Scoreboard finishScore = JsonUtility.FromJson<Scoreboard>(dataAsJson);

            lastScore.text = finishScore.score.ToString();
            lastTime.text = finishScore.time;
        }

    }
	
}
