﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class UpdateScorefromJson : MonoBehaviour {

    public Text finishingScore;
    public Text finishingTime;

    private string path;

	// Use this for initialization
	void Start () {
        path = Application.dataPath + "/JSONscores.json";
        if (File.Exists(path))
        {
            string dataAsJson = File.ReadAllText(path);
            Scoreboard finishScore = JsonUtility.FromJson<Scoreboard>(dataAsJson);

            finishingScore.text = finishScore.score.ToString();
            finishingTime.text = finishScore.time;
        }
        
	}
	
}

