﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {


    public GameObject musicPlayer;

    private Slider slider;
    
    void Start()
    {
        slider = gameObject.GetComponent<Slider>();   
    }
    void Update () {
        musicPlayer.GetComponent<AudioSource>().volume = slider.value;
	}
}
