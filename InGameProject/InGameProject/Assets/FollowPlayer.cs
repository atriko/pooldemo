﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform playerPos;
    public Transform cam;

    //LIMITATION FOR Y AXIS
    private const float Y_MIN = -20.0f;
    private const float Y_MAX = 0.0f;

    //VARIABLES
    public float xSensitivity;
    public float ySensitivity;

    private float offsetZ = 0.4f;
    private float offsetY = 0.4f;
    private float posX = 0.0f;
    private float posY = 0.0f;

    private void Update()
    {
        //GET THE X AND Y AXISES AND LIMIT THE Y WITH CLAMP
        posX += Input.GetAxis("Mouse X") * xSensitivity;
        posY += Input.GetAxis("Mouse Y") * ySensitivity;
        posY = Mathf.Clamp(posY, Y_MIN, Y_MAX);

    }
    //USING LATE UPDATE FOR CAMERA FOLLOW
    void LateUpdate () {
        //PUT THE CAMERA AWAY FROM PLAYER WITH THE OFFSETS
        Vector3 dir = new Vector3(0, offsetY, -offsetZ);
        //GET THE CAMERA ROTATION FROM MOUSE
        Quaternion rot = Quaternion.Euler(posY, posX, 0);
        //ADJUST CAM POSITION FROM THE PLAYER
        cam.position = playerPos.position + rot * dir;
        //LOCK CAMERA TO THE PLAYER
        cam.LookAt(playerPos.position);
	}
}
